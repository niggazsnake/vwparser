# -*- coding: utf-8 -*-
import os
from twisted.internet import reactor
from twisted.web import static, server

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
files = os.path.join(PROJECT_DIR, 'files')
root = static.File(files)
reactor.listenTCP(80, server.Site(root))
reactor.run()
