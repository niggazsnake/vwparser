<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('America/Chicago');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
    'base_url' => '/',
    'index_file' => false,
//	'errors'     => FALSE 
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
     'auth'       => MODPATH.'auth',       // Basic authentication
     'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'captcha'        => MODPATH.'captcha',        // Object Relationship Mapping		
    'email' => MODPATH.'email', 	
    'promo' => MODPATH.'promo', 	
    'orm'        => MODPATH.'orm',        // Object Relationship Mapping
        // 'unittest'   => MODPATH.'unittest',   // Unit testing
        // 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set('rss', 'rss')
        ->defaults(array(
            'controller' => 'rss',
            'action' => 'index',
        ));
		
Route::set('admin/newsparser', 'admin/newsparser')
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'newsparser',
            'action' => 'index',
        ));
		
Route::set('admin/priceparser', 'admin/priceparser')
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'priceparser',
            'action' => 'index',
        ));

Route::set('admin/pricesparser', 'admin/pricesparser')
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'pricesparser',
            'action' => 'index',
        ));

Route::set('sitemap', 'sitemap.xml')
        ->defaults(array(
            'controller' => 'sitemap',
            'action' => 'index',
        ));
 
Route::set('start0', '<model>', array(
            'model' => '(polo|golf|scirocco|polo_sedan|scirocco|touran|touareg)'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'models',
        )); 
 
Route::set('vwex', 'models/volkswagen_exclusive', array(
            
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'highlights',
			'model' => 'volkswagen_exclusive'
        )); 
////////////////////////////////////////////////////
/*Route::set('start1', 'spetspredlozheniya-polo')
        ->defaults(array(
            'model' => 'polo',
            'controller' => 'main',
            'action' => 'special_offers',
        )); */

Route::set('start2', 'spetsialnoe-predlozhenie-na-polo-style')
        ->defaults(array(
            'model' => 'polo',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '186',
        )); 

Route::set('start3', 'polo-v-kredit-pereplata-ot-1-99-kasko-ot-3')
        ->defaults(array(
            'model' => 'polo',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '185',
        )); 

Route::set('start4', 'golf-style-bolshogo-goroda')
        ->defaults(array(
            'model' => 'golf',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '188',
        )); 

Route::set('start5', 'golf-v-kredit-pereplata-ot-1-99')
        ->defaults(array(
            'model' => 'golf',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '187',
        )); 

Route::set('start6', 'golfplus')
        ->defaults(array(
			'model' => 'golf_plus',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start7', 'golf-plus-v-kredit-pereplata-ot-1-99')
        ->defaults(array(
            'model' => 'golf_plus',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '189',
        )); 

Route::set('start8', 'golf-gti')
        ->defaults(array(
			'model' => 'golf_gti',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start9', 'scirocco-v-kredit-pereplata-ot-1-99')
        ->defaults(array(
            'model' => 'scirocco',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '190',
        )); 

Route::set('start10', 'polo-sedan')
        ->defaults(array(
			'model' => 'polo_sedan',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start11', 'spetsialnoe-predlozhenie-na-polo-sedan')
        ->defaults(array(
            'model' => 'polo_sedan',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '192',
        )); 

Route::set('start12', 'spetsialnoe-predlozhenie-ot-volkswagen-credit')
        ->defaults(array(
            'model' => 'polo_sedan',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '191',
        )); 

Route::set('start13', 'jetta')
        ->defaults(array(
			'model' => 'new_jetta',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start14', 'novyy-passat')
        ->defaults(array(
			'model' => 'passat_sedan',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start15', 'models/volkswagen_passat_cc')
        ->defaults(array(
			'model' => 'passat_cc',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start16', 'passat-cc-v-kredit-pereplata-ot-1-99')
        ->defaults(array(
            'model' => 'passat_cc',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '193',
        )); 

Route::set('start17', 'touran-v-kredit-pereplata-ot-1-99')
        ->defaults(array(
            'model' => 'touran',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '195',
        )); 

Route::set('start18', 'novyy-passat-variant')
        ->defaults(array(
			'model' => 'passat_variant',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start19', 'novyy-tiguan')
        ->defaults(array(
			'model' => 'new_tiguan',
            'controller' => 'main',
            'action' => 'models',
        )); 

Route::set('start20', 'grandioznoe-predlozhenie-na-touareg')
        ->defaults(array(
            'model' => 'touareg',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '198',
        )); 

Route::set('start21', 'novyy-touareg-v-kredit-pereplata-ot-1-99-kasko-ot-2-4')
        ->defaults(array(
            'model' => 'touareg',
            'controller' => 'main',
            'action' => 'special_offers',
            'id' => '197',
        )); 

Route::set('start22', 'news/2012/01/10/1221')
        ->defaults(array(
            'controller' => 'finance',
            'action' => 'specials',
            'id' => '405',
        ));


        /**
        DSEO ЧПУ 
        **/
        /*
Одно из условий - разные названия: start3, start4
Route::set('start[N]', 'models/volkswagen_scirocco')
        ->defaults(array(
            'model' => 'scirocco',
            'controller' => 'main',
            'action' => 'models',
        ));
        */
Route::set('start23', 'models/volkswagen_scirocco')
        ->defaults(array(
            'model' => 'scirocco',
            'controller' => 'main',
            'action' => 'models',
        ));
            Route::set('start23', 'models/volkswagen_golf')
        ->defaults(array(
            'model' => 'golf',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start24', 'models/volkswagen_zhuk')
        ->defaults(array(
            'model' => 'beetle',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start25', 'models/volkswagen_touran')
        ->defaults(array(
            'model' => 'touran',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start26', 'models/volkswagen_passat_sedan')
        ->defaults(array(
            'model' => 'passat_sedan',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start27', 'models/volkswagen_polo_sedan')
        ->defaults(array(
            'model' => 'polo_sedan',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start28', 'models/volkswagen_jetta')
        ->defaults(array(
            'model' => 'new_jetta',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start29', 'models/volkswagen_touareg')
        ->defaults(array(
            'model' => 'touareg',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start30', 'volkswagen_s_probegom')
        ->defaults(array(
            'controller' => 'finance',
            'action' => 'used_cars',
        ));
        
Route::set('start31', 'models/volkswagen_tiguan')
        ->defaults(array(
            'model' => 'tiguan',
            'controller' => 'main',
            'action' => 'models',
        ));
Route::set('start32', 'models/volkswagen_scirocco')
        ->defaults(array(
            'model' => 'scirocco',
            'controller' => 'main',
            'action' => 'models',
        ));

        /**
        DSEO ЧПУ 
        **/         


		
/* Вписывать тут 
 * Одно из условий - разные названия: start3, start4
 * 
 * 
 * 
 * 
 */

/////////////////////////////////////////////////////
Route::set('start', 'models(/<model>)')
        ->defaults(array(
            'controller' => 'main',
            'action' => 'models',
        ));


Route::set('modules', 'models/<model>/<directory>(/<action>(/<id>))', array(
            'directory' => '(trimlines)'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'list',
        ));

Route::set('detail', 'models/<model>/<action>(/<id>)', array(
//            'id' => '.+'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'models',
        ));

//Route::set('detail2', 'models/<model>/<controller>(/<action>(/<id>))')
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'models',
//        ));
//Route::set('adminmodel', 'admin/models/<model>', array(
//            'model' => '.+',
//        ))
//        ->defaults(array(
//            'directory' => 'admin',
//            'controller' => 'models',
//            'action' => 'index',
//        ));
//Route::set('adminmodel', 'admin/category/<id>', array(
//            'category' => '.+',
//        ))
//        ->defaults(array(
//            'directory' => 'admin',
//            'controller' => 'category',
//            'action' => 'index',
//        ));
//Route::set('main', 'models/<model>(/<>)', 
//            array('model' => '.+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>)', 
//            array('model' => '[a-z]+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>)', 
//            array('model' => '[a-z]+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>(/<action>(/<id>))))', 
//            array('model' => '[a-z0-9_/]+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>(/<action>(/<id>))))', 
//            array('model' => '.+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('default', '(<controller>(/<action>(/<id>)))')
//        ->defaults(array(
//            'controller' => 'welcome',
//            'action' => 'index',
//        ));
//Route::set('main', 'models/<model>(/<action>(/<id>)))', array('model' => '.+'))
//        ->defaults(array(
//            'controller' => 'welcome',
//            'action' => 'index',
//        ));

Route::set('admin2', 'admin/models/<model>/<action>(/<id>)', array(
            'model' => '.+',
            'id' => '.+',
            'action' => '(gallery|comps|highlights|catalog|spec|service|press|wallpaper|online)'
        ))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'main',
            'action' => 'index',
        ));
//
//Route::set('admin2', 'admin/models/<model>/<action>', array(
//            'model' => '.+',
////            'id' => '.+'
//        ))
//        ->defaults(array(
//            'directory' => 'admin',
//            'controller' => 'main',
//            'action' => 'index',
//        ));

Route::set('adminmodel', 'admin/<action>(/<id>)', array(
        ))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'main',
            'action' => 'index',
        ));

Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))', array(
            'id' => '.+',
        ))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'main',
            'action' => 'index',
        ));

//Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))')
//	->defaults(array(
//                'directory' => 'admin',
//		'controller' => 'main',
//		'action'     => 'index',
//	));

 Route::set('default2', '<directory>/<controller>/<action>(/<id>)', array(
			'directory' => 'trimlines',
            'id' => '.+'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'index',
        )); 

Route::set('default', '(<controller>(/<action>(/<id>)))', array(
            'id' => '.+'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'index',
        ));
		
	
set_exception_handler(array('Exceptionhandler', 'handle'));

