<?php

//defined('SYSPATH') or die('No direct script access.');
class Controller_Admin_Pricesparser extends Controller {
//class Controller_Pricesparser extends Controller_Apptemplate {

    public function action_index(){

//        $path = '/home/u333801/vwparser/sql_data.sql';
        $path = '/var/www/u0198537/data/sql_data2.sql';
        $cars = array();

        $handle = @fopen($path, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {

                $type_query = substr($buffer, 0, strpos($buffer, " "));

                if ($type_query == "SELECT")
                {
                    $rows = DB::query(Database::SELECT, $buffer)->execute();
                    foreach($rows as $row)
                    {
                        if (array_key_exists('name_comp', $row))
                            $car = $row['name_en'] . ' ' . $row['name_comp'];
                        else
                            $car = $row['name_en'];

                        if(!array_key_exists($car, $cars))
                        {
                            $cars[$car]['price_from_old'] = $row['price_from'];
                            $cars[$car]['price_to_old'] = $row['price_to'];
                        }
                        else
                        {
                            $cars[$car]['price_from_new'] = $row['price_from'];
                            $cars[$car]['price_to_new'] = $row['price_to'];
                        }
                    }
                }
                else
                {
                    echo "UPDATE HERE<br>";
                    $rows = DB::query(Database::UPDATE, $buffer)->execute();
                }

            }

            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
        print_r($cars);
        $message = $this->make_respone_to_mail($cars);

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

		mail('notsnake@gmail.com, py@pylabs.ru', 'Цены на сайте vw-nn.ru успешно обновлены', $message, $headers);
    }

    public function make_respone_to_mail($cars){
        $out_html = '<table border="0" cellpadding="6" cellspacing="0" width="100%" style="margin:0; padding:0; max-width: 600px">';
        $out_html .= '<tr><th>Автомобиль</th><th>Старая цена</th><th>Новая цена</th><th>Старая скидка</th><th>Новая скидка</th></tr>';
        foreach($cars as $car=>$params)
        {
            $make_red = '';
            if ($params['price_from_old'] != $params['price_from_new'] or $params['price_to_old'] != $params['price_to_new'])
                $make_red = "style='color:red'";

            $out_html .= sprintf('<tr %s>', $make_red);
            $out_html .= '<td>'.$car.'</td>';
            $out_html .= '<td style="text-align: center;">'.$params['price_from_old'].'</td>';
            $out_html .= '<td style="text-align: center;">'.$params['price_from_new'].'</td>';
            $out_html .= '<td style="text-align: center;">'.$params['price_to_old'].'</td>';
            $out_html .= '<td style="text-align: center;">'.$params['price_to_new'].'</td>';
            $out_html .= '</tr>';
        }
        $out_html .= '</table>';
        return $out_html;
    }

}

?>