<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('America/Chicago');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));	
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
    'base_url' => '/',
	'errors'     => false,
	'profile' => false
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
     'auth'       => MODPATH.'auth',       // Basic authentication
    // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'captcha'        => MODPATH.'captcha',        // Object Relationship Mapping	
	'promo' => MODPATH.'promo',
	'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	'email' => MODPATH.'email' 	
        // 'orm'        => MODPATH.'orm',        // Object Relationship Mapping
        // 'unittest'   => MODPATH.'unittest',   // Unit testing
        // 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
 Route::set('vwex', 'models/volkswagen_exclusive', array(
            
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'highlights',
			'model' => 'volkswagen_exclusive'
        ));  
 
Route::set('start', 'models(/<model>)')
        ->defaults(array(
            'controller' => 'main',
            'action' => 'models',
        ));

Route::set('pricesparser', 'pricesparser')
        ->defaults(array(
            'controller' => 'pricesparser',
            'action' => 'index',
        ));

Route::set('modules', 'models/<model>/<directory>(/<action>(/<id>))', array(
            'directory' => '(trimlines)'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'list',
        ));

Route::set('detail', 'models/<model>/<action>(/<id>)', array(
//            'id' => '.+'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'models',
        ));

//Route::set('detail2', 'models/<model>/<controller>(/<action>(/<id>))')
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'models',
//        ));
//Route::set('adminmodel', 'admin/models/<model>', array(
//            'model' => '.+',
//        ))
//        ->defaults(array(
//            'directory' => 'admin',
//            'controller' => 'models',
//            'action' => 'index',
//        ));
//Route::set('adminmodel', 'admin/category/<id>', array(
//            'category' => '.+',
//        ))
//        ->defaults(array(
//            'directory' => 'admin',
//            'controller' => 'category',
//            'action' => 'index',
//        ));
//Route::set('main', 'models/<model>(/<>)', 
//            array('model' => '.+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>)', 
//            array('model' => '[a-z]+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>)', 
//            array('model' => '[a-z]+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>(/<action>(/<id>))))', 
//            array('model' => '[a-z0-9_/]+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('main', 'models(/<model>(/<action>(/<id>))))', 
//            array('model' => '.+'))
//        ->defaults(array(
//            'controller' => 'main',
//            'action' => 'index',
//        ));
//Route::set('default', '(<controller>(/<action>(/<id>)))')
//        ->defaults(array(
//            'controller' => 'welcome',
//            'action' => 'index',
//        ));
//Route::set('main', 'models/<model>(/<action>(/<id>)))', array('model' => '.+'))
//        ->defaults(array(
//            'controller' => 'welcome',
//            'action' => 'index',
//        ));

Route::set('admin2', 'admin/models/<model>/<action>(/<id>)', array(
            'model' => '.+',
            'id' => '.+',
            'action' => '(gallery|comps|highlights|catalog|spec|service|press|wallpaper|online)'
        ))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'main',
            'action' => 'index',
        ));
//
//Route::set('admin2', 'admin/models/<model>/<action>', array(
//            'model' => '.+',
////            'id' => '.+'
//        ))
//        ->defaults(array(
//            'directory' => 'admin',
//            'controller' => 'main',
//            'action' => 'index',
//        ));

Route::set('adminmodel', 'admin/<action>(/<id>)', array(
        ))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'main',
            'action' => 'index',
        ));

Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))', array(
            'id' => '.+',
        ))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'main',
            'action' => 'index',
        ));

//Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))')
//	->defaults(array(
//                'directory' => 'admin',
//		'controller' => 'main',
//		'action'     => 'index',
//	));

 Route::set('default2', '<directory>/<controller>/<action>(/<id>)', array(
			'directory' => 'trimlines',
            'id' => '.+'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'index',
        )); 

Route::set('default', '(<controller>(/<action>(/<id>)))', array(
            'id' => '.+'
        ))
        ->defaults(array(
            'controller' => 'main',
            'action' => 'index',
        ));
set_exception_handler(array('Exceptionhandler', 'handle'));

