FROM python:2.7

RUN apt-get update && apt-get install -y \
		gcc \
		gettext \
	--no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /app
RUN mkdir -p /app/files
WORKDIR /app

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /app

VOLUME /app/files

EXPOSE 80

CMD ["python", "/app/httpserver.py"]