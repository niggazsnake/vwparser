# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from vwparser.items import VwparserItem
from vwparser.loaders import VwParserLoader


class VwSpider(CrawlSpider):
    name = "vwparser"
    allowed_domains = ["slv.vw-dealer.ru"]
    start_urls = ["http://slv.vw-dealer.ru/models/"]

    rules = (
        Rule(LinkExtractor(deny=("nf.*", ), allow=('/models/\w+/$', r'/comps/$', )), follow=True),
        Rule(LinkExtractor(deny=("nf.*", ), allow=(r'/comps/\w+/$', )), callback='parse_item'),
        Rule(LinkExtractor(deny=("nf.*", ), allow=('/models/$', )), callback='parse_items'),
    )

    def parse_item(self, response):
        l = VwParserLoader(item=VwparserItem(), response=response)
        l.add_xpath('price', u'//table/tbody/tr[descendant::text()="Цена"]/td/text()')
        l.add_xpath('compl', '//h1[@class="margintop"]/text()')
        l.add_xpath('car', '//a[@class="section_label"]/text()')

        return l.load_item()

    def parse_items(self, response):
        models = response.xpath('//a[@class="ml_item"]')
        items = []
        for model in models:
            item = VwparserItem()
            item['price'] = model.xpath('./span[@class="model_price"]/text()').extract()[0]
            item['compl'] = ''
            item['car'] = model.xpath('./span[@class="model_name"]/text()').extract()[0]
            items.append(item)
        return items



