# -*- coding: utf-8 -*-
import os

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

class VwparserPipeline(object):
    models_to_change = {'Polo': 'Polo sedan', 'Jetta': 'New Jetta', u'Новый Passat Alltrack': 'Passat Alltrack',
                        u'Новый Passat Variant': 'Passat Variant', 'Passat': 'Passat sedan',
                        u'Новый Caddy': 'New Caddy', u'Новый Multivan': 'Multivan', u'Новый California': 'California'
                        }
    models_to_change2 = {'Polo': 'Polo sedan', 'Passat': 'Passat sedan', u'Новый Passat Alltrack': 'Passat Alltrack',
                         u'Новый Passat Variant': 'Passat Variant', u'Новый Caddy': 'Caddy',
                         u'Новый Multivan': 'Multivan', u'Новый California': 'California'
                         }

    def __init__(self):
        self.file = open("files/sql_data.sql", "w")
        self.file2 = open("files/sql_data2.sql", "w")

        self.prices_for_sql = {'price_from': 0, 'price_to': 0}

    def find_prices(self, str_price):

        if str_price.find(u'Спец. цена') > 0:
            self.prices_for_sql['price_from'] = int(str_price[3:str_price.find(u'руб') - 1].replace(" ", ""))
            self.prices_for_sql['price_to'] = int(
                str_price[str_price.find(u'Преимущество до') + 16:str_price.find(u'руб. **')].replace(" ", ""))
        else:
            self.prices_for_sql['price_from'] = int(str_price[3:str_price.find(u'руб') - 1].replace(" ", ""))
            self.prices_for_sql['price_to'] = '0'

    def price_for_model(self, str_price):
        price = str_price[3:str_price.find(u' руб.')].replace(" ", "")
        return price

    def sql_to_file(self, compl, model, file):
        if compl != '':
            sql_select = "SELECT m.name_en, c.name_comp, c.price_from, c.price_to FROM tb_models m " \
                         "INNER JOIN tb_comps c ON m.id=c.id_model " \
                         "WHERE m.visible=1 and name_en = '%s' and (name_comp='%s' or name_comp='%s');" \
                         % (model, compl, model + " " + compl)

            sql_update = "UPDATE tb_comps c SET c.price_from='%s', c.price_to='%s' " \
                         "WHERE ((c.price_from != '%s') OR (c.price_to != '%s')) AND " \
                         "((c.name_comp='%s') or (c.name_comp='%s')) AND " \
                         "c.id_model IN (SELECT id from tb_models m WHERE m.visible=1 AND m.name_en='%s');" \
                         % (self.prices_for_sql['price_from'], self.prices_for_sql['price_to'],
                            self.prices_for_sql['price_from'], self.prices_for_sql['price_to'], compl,
                            model + " " + compl, model)
        else:
            sql_select = "SELECT name_en, price_from, price_to FROM tb_models " \
                         "WHERE visible=1 and name_en = '%s';" % model

            sql_update = "UPDATE tb_models SET price_from='%s' " \
                         "WHERE price_from != '%s' AND visible=1 AND name_en='%s';" \
                         % (self.prices_for_sql['price_from'], self.prices_for_sql['price_from'], model)

        sql_update = sql_update.encode(encoding="utf-8")
        sql_select = sql_select.encode(encoding="utf-8")

        file.write(sql_select + '\n')
        file.write(sql_update + '\n')
        file.write(sql_select + '\n')

    def create_sql_query_for_car(self, car):

        model = car['car']
        compl = car['compl']
        price = car['price']
        model2 = model

        if model in self.models_to_change:
            model = self.models_to_change[model]

        if model2 in self.models_to_change2:
            model2 = self.models_to_change2[model2]

        if compl != '':
            self.find_prices(price)
        else:
            self.prices_for_sql['price_from'] = self.price_for_model(price)

        if self.prices_for_sql['price_from'] > 0:
            self.sql_to_file(compl, model, self.file)
            self.sql_to_file(compl, model2, self.file2)

    def process_item(self, item, spider):
        self.create_sql_query_for_car(item)
        return item

    def close_spider(self, spider):
        # self.file.write("COMMIT;" + '\n')
        self.file.close()
        self.file2.close()

