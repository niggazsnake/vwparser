# -*- coding: utf-8 -*-

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join, MapCompose


class VwParserLoader(ItemLoader):
    default_output_processor = TakeFirst()
    compl_in = MapCompose(lambda string: string.replace('&', 'and') if string.find('&') > 0 else string)
    #price_in = MapCompose(lambda string: str(string))
    price_out = Join()
